package com.mmiholdings.sandpit.javaee.notes;

import com.mmiholdings.sandpit.javaee.notes.event.EventType;
import com.mmiholdings.sandpit.javaee.notes.event.Notify;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import javax.cache.Cache;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.extern.java.Log;

@Stateless
@Log
public class NotesService {
    
    @Inject
    private Cache<String,Note> cache;
    
    //<editor-fold defaultstate="collapsed" desc="interceptor">
    @Notify(type = EventType.CREATE)
    //</editor-fold>
    public Note createNote(//<editor-fold defaultstate="collapsed" desc="bean-validation">
            @NotNull @Size(min=2, max=20) //</editor-fold>
            String title,@NotNull String text) throws NoteExistAlreadyException{
        
        if(exist(title))throw new NoteExistAlreadyException();
        Note note = new Note(title, text);
        save(note);
        log.log(Level.INFO, "Created note [{0}]", note);
        return note;
    }

    public Note getNote(//<editor-fold defaultstate="collapsed" desc="bean-validation">
            @NotNull @Size(min=2, max=20) //</editor-fold>
            String title) throws NoteNotFoundException{
        
        if(!exist(title))throw new NoteNotFoundException();
        Note note = cache.get(title);
        log.log(Level.INFO, "Retrieving note [{0}]", note);
        return note;
    }

    //<editor-fold defaultstate="collapsed" desc="interceptor">
    @Notify(type = EventType.DELETE)
    //</editor-fold>
    public Note deleteNote(//<editor-fold defaultstate="collapsed" desc="bean-validation"> 
            @NotNull @Size(min=2, max=20) //</editor-fold>
            String title) throws NoteNotFoundException{
        if(!exist(title))throw new NoteNotFoundException();
        Note note = cache.get(title);
        cache.remove(title);
        log.log(Level.INFO, "Removing note [{0}]", title);
        return note;
    }

    //<editor-fold defaultstate="collapsed" desc="interceptor">
    @Notify(type = EventType.UPDATE)
    //</editor-fold>
    public Note updateNote(@NotNull Note note) throws NoteNotFoundException{
        
        if(!exist(note.getTitle()))throw new NoteNotFoundException();
        save(note);
        log.log(Level.INFO, "Updated note [{0}]", note);
        return note;
    }

    public boolean exist(//<editor-fold defaultstate="collapsed" desc="bean-validation"> 
            @NotNull @Size(min=2, max=20) //</editor-fold>
                    String title){
        
        return cache.containsKey(title);
    }

    public List<String> getNoteTitles(){
        List<String> titles = new ArrayList<>();
        Iterator<Cache.Entry<String, Note>> iterator = cache.iterator();
        while (iterator.hasNext()) {
            Cache.Entry<String, Note> next = iterator.next();
            titles.add(next.getKey());
        }
        return titles;
    }
    
    private void save(Note note){
        cache.put(note.getTitle(), note);
    }
    
}