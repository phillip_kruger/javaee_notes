package com.mmiholdings.sandpit.javaee.notes.event;

import com.mmiholdings.sandpit.javaee.notes.Note;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class ChangeEvent {
    private Note note;
    private EventType type;
}
