# Notes Application #
## Example application to demonstrate Java EE ##

This is a simple application that allows create, read , update and delete notes. It use the following specifications and libraries:

* EJB ([JSR 345](https://www.jcp.org/en/jsr/detail?id=345)) 
* POJO with [Lombok](https://projectlombok.org/)
* JCache ([JSR 107](https://www.jcp.org/en/jsr/detail?id=107))
* JAXB ([JSR 222](https://www.jcp.org/en/jsr/detail?id=222))
* Bean validation ([JSR 349](https://www.jcp.org/en/jsr/detail?id=349))
* JAX-RS ([JSR 339](https://www.jcp.org/en/jsr/detail?id=339))
* [Swagger](http://swagger.io/) and [Swagger UI](http://swagger.io/swagger-ui/)
* JAX-WS ([JSR 224](https://www.jcp.org/en/jsr/detail?id=224))
* Interceptors ([JSR 345](https://www.jcp.org/en/jsr/detail?id=345)) 
* CDI Events ([JSR 346](http://www.cdi-spec.org/))
* Web sockets ([JSR 356](https://www.jcp.org/en/jsr/detail?id=356))
* [Semantic-UI](https://semantic-ui.com/) (via [webjars](http://www.webjars.org/))