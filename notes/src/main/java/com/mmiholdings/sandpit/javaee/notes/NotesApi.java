package com.mmiholdings.sandpit.javaee.notes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;
import java.util.List;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

//<editor-fold defaultstate="collapsed" desc="jax-rs">
@Path("/note")
@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML}) @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="swagger">
@SwaggerDefinition (info = @Info (
                        title = "Notes Service",
                        description = "A simple reminder notes application",
                        version = "1.0-SNAPSHOT",
                        contact = @Contact (
                            name = "Client Engagement Solutions Team", 
                            email = "BTSJava@momentum.co.za", 
                            url = "http://git-ces.mmih.biz/Sandpit/javaee"
                        )
                    )
                )
@Api(value = "Notes service")
//</editor-fold>
//<editor-fold defaultstate="collapsed" desc="jax-ws">
@WebService
//</editor-fold>
public class NotesApi {
    
    @Inject 
    private NotesService notesService;
    
    //<editor-fold defaultstate="collapsed" desc="jax-rs">
    @POST
    @Path("/{title}")
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="swagger">
    @ApiOperation(value = "Create a new note", notes = "This will create a new note under a certain title, only if it does not exist already")
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="jax-ws">
    @WebMethod
    //</editor-fold>
    public Note createNote(@WebParam @PathParam("title") String title,@NotNull @WebParam String text) throws NoteExistAlreadyException{
        return notesService.createNote(title, text);
    }
    
    //<editor-fold defaultstate="collapsed" desc="jax-rs">
    @GET
    @Path("/{title}")
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="swagger">
    @ApiOperation(value = "Retrieve a note", notes = "This will find the note with a certain title, if it exist")
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="jax-ws">
    @WebMethod
    //</editor-fold>
    public Note getNote(@PathParam("title") @WebParam String title) throws NoteNotFoundException{
        return notesService.getNote(title);
    }
    
    //<editor-fold defaultstate="collapsed" desc="jax-rs">
    @DELETE
    @Path("/{title}")
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="swagger">
    @ApiOperation(value = "Delete a note", notes = "This will delete the note with a certain title, if it exist")
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="jax-ws">
    @WebMethod
    //</editor-fold>
    public void deleteNote(@PathParam("title") @WebParam String title) throws NoteNotFoundException{
        notesService.deleteNote(title);
    }
    
    //<editor-fold defaultstate="collapsed" desc="jax-rs">
    @PUT
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="swagger">
    @ApiOperation(value = "Update a note", notes = "This will update the note with a given new note, if it exist")
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="jax-ws">
    @WebMethod
    //</editor-fold>
    public Note updateNote(@WebParam Note note) throws NoteNotFoundException{
        return notesService.updateNote(note);
    }
    
    //<editor-fold defaultstate="collapsed" desc="jax-rs">
    @GET
    @Path("/exists/{title}")
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="swagger">
    @ApiOperation(value = "Check if note exist", notes = "This will check if a note with a certain title exist")
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="jax-ws">
    @WebMethod
    //</editor-fold>
    public boolean exist(@WebParam @PathParam("title") String title){
        return notesService.exist(title);
    }
    
    //<editor-fold defaultstate="collapsed" desc="jax-rs">
    @GET
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="swagger">
    @ApiOperation(value = "Get all the note titles", notes = "This will return all current note titles")
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="jax-ws">
    @WebMethod
    //</editor-fold>
    public List<String> getNoteTitles(){
        return notesService.getNoteTitles();
    }
}