package com.mmiholdings.sandpit.javaee.notes;

import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

 
@Data @NoArgsConstructor @AllArgsConstructor
//<editor-fold defaultstate="collapsed" desc=".">
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
//</editor-fold>
public class Note implements Serializable {
    private static final long serialVersionUID = -8531040143398373846L;
    //<editor-fold defaultstate="collapsed" desc=".">
    @NotNull @XmlAttribute(required=false) 
    //</editor-fold>
    private Date created = new Date();
    //<editor-fold defaultstate="collapsed" desc=".">
    @NotNull @XmlAttribute(required=false) 
    //</editor-fold>
    private Date lastUpdated = new Date();
    //<editor-fold defaultstate="collapsed" desc=".">
    @NotNull @XmlAttribute(required=true)
     //</editor-fold>
    private String title;
    //<editor-fold defaultstate="collapsed" desc=".">
    @NotNull @XmlAttribute(required=true) 
    //</editor-fold>
    private String text;
    
    public Note(@NotNull String title, @NotNull String text){
        this.created = new Date();
        this.lastUpdated = new Date();
        this.title = title;
        this.text = text;
    }
    
}
